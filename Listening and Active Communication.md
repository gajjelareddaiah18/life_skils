Question 1 What are the steps/strategies to do Active Listening? (Minimum 6 points)
--
- Pay attention to the speaker and what they are saying.
- Avoid interrupting until they have finished speaking.
- Reflect back what the speaker said to ensure understanding.
- Ask clarifying questions to get a better grasp of the speaker's message.

Question 2.According to Fisher's model, what are the key points of Reflective Listening? (Write in your own words, use simple English)
--
 ### Reflective Listening involves:
- Giving full attention to the speaker.
- Repeating or summarizing what the speaker said to ensure understanding.
  
Question 3. What are the obstacles in your listening process?
--
- Personal thoughts or reflections.
- External distractions.
- Social media and video games.

Question 4. What can you do to improve your listening?
--
- Practice active listening techniques.
- Minimize distractions.
- Seek feedback on your listening skills.
- Be mindful of your own biases and assumptions.
 
Question 5. When do you switch to Passive communication style in your day to day life?
--
- To avoid conflict.
- To maintain relationships without taking on added responsibility.
 
Question 6. When do you switch into Aggressive communication styles in your day to day life
--
- When feeling like one's rights are being violated.
- When defending oneself.
  
Question 7. When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?
--
- When feeling disrespected.
- To avoid discussions on sensitive matters.
- When experiencing jealousy or insecurity.
  
Question 8. How can you make your communication assertive?
-- 
- Express thoughts, feelings, and needs clearly and respectfully.
- Stay within one's comfort zone while expressing oneself.