What Causes Sexual Harassment?
--
- Unwanted advances or comments of a sexual nature.
- Sharing explicit content or making sexual jokes.
- Touching, groping, or unwelcome physical contact.
- Cyberbullying with explicit messages or images.
- Creating an intimidating environment with persistent sexual comments.

What to Do When Facing or Witnessing Sexual Harassment
--
 - **Up**: Politely tell the person that their behavior is unacceptable if you feel safe doing so.
 - **Record** Incidents: Keep a record of what happened, including dates, times, and any witnesses.
 - **Report It**: Inform your supervisor, HR, or the appropriate authority using your workplace's reporting procedures.
 - **Seek Support**: Talk to friends, family, or a counselor for emotional support.
 - **Know Your Rights**: Understand your legal rights and protections against sexual harassment.
  
Understanding and Dealing with Sexual Harassment
--
Different Scenarios of Sexual Harassment
--
- **Unwanted Advances**: When someone makes unwelcome romantic or sexual overtures.
- **Inappropriate Comments**: Sharing sexually explicit jokes or comments.
- **Physical Contact**: Touching, groping, or any unwanted physical contact.
- **Online Harassment**: Sending explicit messages or images without consent.
- **Hostile Environment**: Creating an intimidating atmosphere with persistent sexual comments.
  
Handling Cases of Harassment
--
- **Speak Up**: If safe, directly tell the person their behavior is unacceptable.
- **Document Incidents**: Keep records with dates, times, and witnesses.
- **Report It**: Notify HR, your supervisor, or the appropriate authority following workplace protocols.
- **Seek Support**: Talk to friends, family, or a counselor for emotional help.
- **Know Your Rights**: Learn your legal protections against sexual harassment.
  
Behaving Appropriately
--
- **Respect Boundaries**: Always ask for consent before engaging in physical or sexual activities.
- **Use Appropriate Language**: Avoid explicit jokes, comments, or content that might make others uncomfortable.
- **Listen and Respond**: If someone tells you their discomfort, acknowledge it and adjust your behavior.
- **Report Concerns**: If you witness harassment, report it to the right authority.
- **Support Victims**: Show empathy and help them access resources to address the issue.