SOLID: Principles of Object Oriented Design
--
Introduction
--
**SOLID** is a set of rules that help people who write software make it better and easier to work with. It's like a handy guide for building software that's simple to understand, change, and add new things to.

**SOLID** stands for:
- S - Single-responsibility Principle
- O - Open-closed Principle
- L - Liskov Substitution Principle
- I - Interface Segregation Principle
- D - Dependency Inversion Principle


Single-responsiblity Principle
--
Each part of your code should perform only one task, akin to having tools in a toolbox, each with its specific job
```
function getUserInfo(user) {
  return `${user.name} <${user.email}>`;
}
function sendEmail(user, message) {
  console.log(`Sending email to ${user.email}: ${message}`);
}
const user = { name: "John Doe", email: "john@example.com" };
const userInfo = getUserInfo(user);
sendEmail(user, "Hello, John! This is a test email.");
//output:Sending email to john@example.com: Hello, John! This is a test email.

```

Open-closed Principle
--
OCP says you should be able to add new things to your code without changing what's already there. It's like adding new Lego pieces to a structure without having to rebuild the whole thing.
```
const calculateArea = shape => shape.area();

// Example usage
const rectangle = { width: 5, height: 10, area: () => rectangle.width * rectangle.height };
console.log("Area:", calculateArea(rectangle));//Area: 50


```
Liskov Substitution Principle
--
LSP means you can use a subclass in place of the parent class without issues. It's like swapping different shapes in a drawing without messing up the picture.
```
class Shape { area() {} }
class Square extends Shape { area() { return this.side * this.side; } }

const printArea = shape => console.log("Area:", shape.area());

// Example usage
const square = { side: 5, area: () => square.side * square.side };
printArea(square);    // Output: Area: 25

```
Interface Segregation Principle
--
ISP means you should only use what you need in a program. It's like choosing dishes you want from a menu, rather than having to order everything.
```
const simplePrinter = {
  print: () => console.log("Printing document...")
};

// Simple implementation of a scanner
const simpleScanner = {
  scan: () => console.log("Scanning document...")
};

// Example usage
simplePrinter.print(); // Output: Printing document...
```

Dependency Inversion Principle
--
DIP says important parts of code should be more general, not too detailed, so it's easier to make changes. It's like upgrading a car's engine without needing to rebuild the whole car because the engine is separate from the rest.
```
const NotificationService = { sendNotification: () => { throw new Error('Method not implemented'); }};
const EmailNotification = { sendNotification: message => console.log("Email notification sent:", message) };
const notificationManager = { notify: message => EmailNotification.sendNotification(message) };

// Example usage
notificationManager.notify("Hello, world!"); // Output: Email notification sent: Hello, world!

```

Conclusion
--
SOLID principles offer guidelines for writing improved software. By following these principles, developers can create code that is easier to comprehend, manage, and expand. Each principle, such as Single Responsibility or Dependency Inversion, contributes significantly to the construction of resilient and adaptable software systems.

References
--
- [DiditalOcean blocks](https://www.digitalocean.com/community/conceptual-articles/s-o-l-i-d-the-first-five-principles-of-object-oriented-design#liskov-substitution-principle)
- [Baeldung](https://www.baeldung.com/solid-principles)
- [medium](https://medium.com/backticks-tildes/the-s-o-l-i-d-principles-in-pictures-b34ce2f1e898)