Question 1. What are the activities you do that make you relax - Calm quadrant?
--
- Relaxation activities include yoga, meditation, deep breathing, and listening to music.


Question 2. When do you find getting into the Stress quadrant?
--
- Stress may also result from challenging situations such as exams, job interviews, experiencing a lot of tasks, having marital problems, or feeling overloaded.


Question 3. How do you understand if you are in the Excitement quadrant?
--
- The excitement quadrant is connected with feelings of engagement, motivation, and excitement. Your pulse may pump confidently, and you'll frequently be itching to do new things while pursuing your goals.


Question 4. Paraphrase the Sleep is your Superpower video in your own words in brief. Only the points, no explanation.
--
- Sleep is essential for memory and learning. These processes are hampered, health is compromised, and memory loss may be increased by lack of sleep. Gene activity is affected by sleep length.


Question 5. What are some ideas that you can implement to sleep better?
--
- Maintain a consistent sleep routine.
- Before going to bed, stay away from phones and TV.
- Establish a relaxing evening activity.
- Make your bedroom dark.



Question 6. Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points, only the points.
--
- Physical activity helps your brain get smarter.
- When you move your body, your brain changes for the positive.
- Regular exercise positively impacts your brain's functions.
- The advantages of physical activity in reshaping brain functions.



Question 7. What are some steps you can take to exercise more?
--
- To exercise more, start by setting clear goals and creating a regular schedule.
Choose activities you enjoy and find a workout buddy for motivation.