Question 1. Which point(s) were new to you?
--
- Over-communicate: Keep the team informed of changes, issues, and progress. Use group channels, answer calls, and enable video in meetings.
- Ask Questions Effectively: Clearly explain problems, mention attempted solutions, and use visuals or code snippets when seeking help.
- Be Mindful of Others: Use appropriate communication mediums, send consolidated messages, and be responsive to streamline conversations.


Question 2. Which area do you think you need to improve on? What are your ideas to make progress in that area?
--
- I think I need to get better at asking questions. To improve, I'll make sure I explain my problems clearly when I'm stuck and mention what I've tried.
- I'll also use pictures or diagrams to help explain. I'll share code and project details with tools like Loom or GitHub gists in an easier way.