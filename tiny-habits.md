1.In this video, what was the most interesting story or idea for you?
--
The most intriguing idea from the video was the emphasis on starting small when trying to establish new habits, rather than attempting significant changes right away.

2.How can you use B = MAP to make making new habits easier? What are M, A and P.B = MAP simplifies habit 
 --
B = MAP Framework: Utilize Behavior (B) influenced by Motivation (M), Ability (A), and Prompt (P) to make habit formation easier.
- **Boost Motivation**: Connect behavior to existing interests or values.
- **Enhance Ability**: Start with small actions to lower the barrier.
- **Establish Prompts**: Set reminders or cues for consistency.
  
3.Why it is important to "Shine" or Celebrate after each successful completion of habit? (This is the most important concept in today's topic. Whatever you celebrate becomes a habit)
--
Celebrating successful habit completion fosters a positive association with the behavior, encouraging its repetition in the future.

4.In this video, what was the most interesting story or idea for you?
--
The idea that small, consistent improvements can lead to significant success over time was captivating. It emphasizes the importance of incremental progress.

 5.What is the book's perspective about Identity?
 --
- The book's perspective on identity is that it plays a significant role in habit formation and behavior change.
- The book says that if you want to change your habits, it's really powerful to change how you see yourself. Instead of only thinking about outside goals, you should make your habits match the kind of person you want to be.

6.Write about the book's perspective on how to make a habit easier to do?
--
Starting with a simple version of the habit, like a two-minute task, makes it easier to begin and increases motivation.

7.Write about the book's perspective on how to make a habit harder to do?
--
The book offers practical methods to make bad habits harder, such as creating obstacles, hiding tempting items, and using commitment tools.

8.Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?
--
- Set a specific time and place for meditation.
- Allocate 10 minutes every morning upon waking.
- Find a meditation style that resonates.

9.Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?
--
- Remove social media apps from the main screen of the phone.
- Set specific time restrictions on social media usage.
- Engage in alternative activities to replace social media browsing..




